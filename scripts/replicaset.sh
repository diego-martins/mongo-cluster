#!/bin/bash

set -m

printf "\n=> Setting replicaset...\n"

printf "\n=> Start with auth...\n"

mongod \
  --bind_ip_all \
  --dbpath $MONGO_DB_PATH \
  --logpath $MONGO_LOG_PATH \
  --storageEngine $MONGO_STORAGE_ENGINE \
  --keyFile $MONGO_KEYFILE \
  --replSet $MONGO_REPLICASET \
  --smallfiles \
  --auth &

printf "\n=> Waiting...\n"

sleep 3

printf "\n=> Initiate replicaset...\n"
mongo admin -u $MONGO_ROOT_USER -p $MONGO_ROOT_PASSWORD --eval "rs.initiate({_id: '$MONGO_REPLICASET', members: [{_id: 0, host: '$MONGO_PRIMARY'}]});"

if [ -n "$MONGO_SECONDARY_1" ]; then
  printf "\n=> Add secondary 1...\n"
  mongo admin -u $MONGO_ROOT_USER -p $MONGO_ROOT_PASSWORD --eval "rs.add('$MONGO_SECONDARY_1');"
fi

if [ -n "$MONGO_SECONDARY_2" ]; then
  printf "\n=> Add secondary 2...\n"
  mongo admin -u $MONGO_ROOT_USER -p $MONGO_ROOT_PASSWORD --eval "rs.add('$MONGO_SECONDARY_2');"
fi

printf "\n=> Shutdown mongo...\n"

mongod --dbpath $MONGO_DB_PATH --shutdown